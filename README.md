Tarefa de Programação: Descoberta automática do servidor

server.py
A classe Directory implementa um servidor, portanto herda a rpyc
Exporta os metodos append e value, onde o primeiro é utlizado pelo cliente para anexar um item na lista e o segundo é utilizado para obter o valor da lista
A main faz a conexão com o servidor de diretório para fazer o registro
A biblioteca de sockets é utilizada apenas para obter o endereço ip do servidor de aplicação
Ao final é feito o registro atraves do metodo register passando como parâmetro o nome do servidor, o ip e a porta e o servidor é iniciado

client.py
O cliente faz a conexão com o servidor de diretório atraves do rpyc.connect usando o ip e a porta
O cliente utiliza o servidor de diretório para fazer o lookup do servidor de apliacação onde ele obtem o endereço ip e a porta do servidor de apliacação
O cliente faz a conexão com o servidor de apliacação
O cliente utiliza o metodo append para adicionar os valores na lista
O cliente imprime o metodo value que obtem os valores da lista completa

constRPYC.py - Arquivo de constantes
Contém o endereço ip e a porta do servidor de diretório

directory-server.py
A classe Directory implementa um servidor, portanto herda a rpyc
Exporta os metodos register e lookup, onde o primeiro é utlizado pelo servidor para fazer o registro e o segundo é utilizado pelo cliente para fazer lookup
O metodo register armazena na tabela registry uma nova entrada, onde o nome do servidor é o índice associado ao endereço ip e porta
Retorna uma mensagem de sucesso quando o registro é realizado
O método lookup é usado para que os clientes possam ter acesso ao registro (endereço ip e porta) associado ao nome do servidor
A main cria uma nova thread no servidor e a inicia para que possa aceitar as conexões